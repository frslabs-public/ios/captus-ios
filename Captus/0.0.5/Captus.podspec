#
# Be sure to run `pod lib lint Captus.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'Captus'
  s.version          = '0.0.5'
  s.summary          = 'Captus is used for OCR'
  s.homepage         = 'https://gitlab.com/frslabs-public/ios/captus-ios'
  s.license          = 'MIT'
  s.author           = { 'ashish' => 'ashish@frslabs.com' }
  s.source           = { :http => 'https://captus-ios.repo.frslabs.space/captus-ios/0.0.5/Captus.framework.zip'}
  s.platform         = :ios
  s.ios.deployment_target = '11.0'
  s.ios.vendored_frameworks = 'Captus.framework'
  s.swift_version = '5.0'
end
